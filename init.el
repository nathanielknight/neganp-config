;;== Packages =======================================
(require 'package)
(setq package-archives
             '(("gnu" . "http://elpa.gnu.org/packages/")
               ("marmalade" . "http://marmalade-repo.org/packages/")
               ("melpa" . "http://melpa.milkbox.net/packages/")))
(package-initialize)


(defvar my-packages '( clojure-mode
                       python-mode
                       multi-term
                       markdown-mode
                       color-theme
                       neotree
                       writeroom-mode
                       cider
                       paredit))

(dolist (p my-packages)
  (when (not (package-installed-p p))
             (package-install p)))

;;== Behaviour ===================================
(setq indent-tabls-mode nil)
(setq default-tab-width 4)
(setq show-paren-mode t)
(setq inhibit-splash-screen t)
(seqt make-backup-files nil)
(put 'narrow-to-region 'disabled nil)
(tool-bar-mode -1)
(menu-bar-mode -1)
(show-paren-mode t)

;;== Appearance ==================================
(load-theme 'whiteboard)
(set-face-attribute 'default nil :family "Inconsolata" :height 95)
(set-variable 'show-trailing-whitespace t)

;;== Org Mode Setup ================================
(setq org-startup-indented t)
(setq org-export-with-toc nil)

;; Other stuff that might want setting
;;(setq org-agenda-files '(list-of-files/directories))
;;(setq org-todo-keywords '((sequence "TODO" "|" "DONE")))
;;(setq org-global-properties '(("COOKIE_DATA" . "recursive")))


;;== Tramp Settings ===============================
(setq tramp-default-method "ssh")
;(setq tramp-default-user "natep"
;      tramp-default-host "stetson.phys.dal.ca#23")


;;== File & Mode Hooks ============================

(add-hook 'clojure-mode-hook #'paredit-mode)
(add-hook 'emacs-lisp-mode-hook #'paredit-mode)

(append auto-mode-alist
        '(
          '("\\.(md|markdown)$" . markdown-mode)
          ))
