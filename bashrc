#  Nat Egan-Pimblett's BASH config file 
# (for use on stetson where there's no zsh)

#qrsh shenanigans
QRSH_WRAPPER="/bin/bash --rcfile=/data1/natep/.bashrc"
export QRSH_WRAPPER

#Aliases
#--shorter versions of common commands and options
alias l="ls"
alias ll="ls -l"
alias la="ls -a"
alias pe="ps -e"
alias time="/usr/bin/time"
alias vi="vim"
alias pig="/data1/natep/.utils/gchem/build/scripts-2.7/pig"
#git contractions
alias gs="git status"
alias gd="git diff"

#dogegit
alias wow="git status"
alias such="git"
alias very="git"

#netcdf paths for geos-chem
ROOT_LIBRARY_DIR="/software"
GC_INCLUDE=${ROOT_LIBRARY_DIR}/include
GC_LIB=${ROOT_LIBRARY_DIR}/lib
GC_BIN=${ROOT_LIBRARY_DIR}/bin
export GC_INCLUDE
export GC_LIB
export GC_BIN


#Increased stacksize
export KMP_DUPLICATE_LIB_OK=TRUE
export KMP_STACKSIZE=2097152000

#Python path
PYTHONPATH="/data1/natep/.utils/gkuhl-gchem-503d114:/usr/lib:/data1/natep/.utils"
export PYTHONPATH

#Most recent ifortran compiler
ifort="/software/intel/composer_xe_2011_sp1.6.233/bin/ifort"
export ifort

#Colours in pager
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

alias terminal="terminal --geometry=88x50"
#alias lpython="/data1/natep/.utils/bin/python"
#alias python="/software/bin/python"
alias ctags="/data1/natep/local/bin/ctags"

#Extend Path
PATH=$PATH":/software/bin:/data1/natep/.utils/"
export PATH

#Git prompt
function _git_prompt() {
    local git_status="`git status -unormal 2>&1`"
    if ! [[ "$git_status" =~ Not\ a\ git\ repo ]]; then
        if [[ "$git_status" =~ nothing\ to\ commit ]]; then
            local ansi=42
        elif [[ "$git_status" =~ nothing\ added\ to\ commit\ but\ untracked\ files\ present ]]; then
            local ansi=43
        else
            local ansi=45
        fi
        if [[ "$git_status" =~ On\ branch\ ([^[:space:]]+) ]]; then
            branch=${BASH_REMATCH[1]}
            test "$branch" != master || branch=' '
        else
            # Detached HEAD.  (branch=HEAD is a faster alternative.)
            branch="(`git describe --all --contains --abbrev=4 HEAD 2> /dev/null ||
                echo HEAD`)"
        fi
        echo -n '\[\e[0;37;'"$ansi"';1m\]'"$branch"'\[\e[0m\] '
    fi
}
function _prompt_command() {
    PS1="`_git_prompt`"'\[\e[0;32m\]\u@\h\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;32m\]\$ \[\e[m\]\[\e[1;37m\]'
}
PROMPT_COMMAND=_prompt_command
export PROMPT_COMMAND
