"   the vimrc file of
"   Author: Nathaniel Egan-Pimblett
"   Email: nathaniel.ep@gmail.com  
"   Comments: this is my VIMRC file.  It's mostly at the point of setting the
"   appearance and the behaviour of basic stuff like line breaking, tabspaces and
"   the like.  Some basic functions/macros are implemented as well.

""Appearance
filetype indent plugin on "autodetect filetype
set mouse=a "allow mouse use (it's the 21st century, after all)
set showcmd "show commands as they're entered

"colors
syntax enable
colorscheme default

"ui modifications
set scrolloff=3 " keep some space at the top and bottom
set showmode
set cursorline
set ruler
set number  	"display line numbers
set cursorcolumn
hi CursorColumn ctermbg=0


"" Mechanics
"general
set backspace=indent,eol,start "allow backspace over everything in insert mode
set nocompatible "we're on a modern system; no legacy compatability
set ttyfast "fast for terminal emulators

"tab completion
set wildmenu " better autocompletion
set wildmode=list:longest

"indents and tabs
set autoindent 	"autoindent
set smartindent	"and be smart about it
set sw=4 		"use python autoindents
set ts=4		"use python tab spacing
set expandtab "insert spaces instead of tabs

"line breaking
"I'm going to try the 'consenting adult' approach for a sec here
"set textwidth=79

"ignore case when searching
set ignorecase
set smartcase
"highlight search terms
set hlsearch

"history and Undo
set history=1000
set undolevels=1000

"folding
set foldmethod=marker
highlight Folded ctermbg=0 ctermfg=130
hi! link FoldColumn Normal
set foldcolumn=2
set foldtext=MyFoldtext()
function! MyFoldtext()
    "foldtext that's indented (helps visual flow)
    let line=getline(v:foldstart)
    let lead = matchstr(line, '^\s*')
    let n = v:foldend - v:foldstart + 1
    let m =  matchstr(line, '[^\s].*')
    return lead . '+ ' . n . ' lines: ' . m
endfunction


""Tags(using exuberant ctags)
"configure plugin
let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_Inc_Winwidth = 0
let Tlist_Exit_OnlyWindow = 1
map <F12> :TlistToggle<CR>
map <F11> :!ctags -R<CR> 
"might need to add more tags (see ctags --list-kinds etc. for details)


""Keybindings
"extra colon
nmap ; :
"execute self
map <F2> :! ./%  <CR>
"toggle spell-check
map <F3> :set invspell<CR>
"wordcount
map <F4> :w !detex \| wc -w<CR>
map <F8> "=strftime("%c")<CR>P
"leader key bindings
map <Leader>c :noh<CR>


"Filetype Specific Settings
filetype on
"latex
let g:tex_flavor='latex' "prevents vim 7.0 and higher from setting type to plaintext
"fortran
if &filetype=="fortran"
    let fortran_do_enddo=1
endif
"ruby
if &filetype=="ruby"
    echo "doing ruby"
    set sw=2 ts=2
endif
"clojure
if &filetype=="clojure"
    echo "doing clojure"
    set sw=2 ts=2
endif
"haskell
if &filetype=="haskell"
    echo "doing haskell"
    set sw=2 ts=2
endif

""Set my home direcory files
let $VIM="~/.vim"
