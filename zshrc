# Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load from ~/.oh-my-zsh/themes/
export ZSH_THEME="simple"

#Settings
export CASE_SENSITIVE="true"
export DISABLE_AUTO_UPDATE="true"
plugins=(git emacs-mode)

source $ZSH/oh-my-zsh.sh
# What follows is my shell rc stuff. (i.e. not zshrc stuff)




#Default programs
export BROWSER='google-chrome'
export EDITOR='emacs'

#Utilities directory
UTIL=/home/neganp/.utils/

#== Aliases =====================
alias ls="ls --color=auto"
alias la="ls -a"
alias pe="ps -e"

##tell me off for being reflexive
pmesg="You\'re not in vi anymore you drongo!"
alias q="echo $pmesg"
alias wq="echo $pmesg"

#Add utils to the python path
export PYTHONPATH="/home/neganp/.utils/:/home/neganp/.utils/gchem"

#Add utils to the system path
PATH+=":"$HOME"/bin"
PATH+=":"$HOME"/.utils"
PATH+=":"$HOME"/.rvm/bin" # Add RVM to PATH for scripting
PATH+=":"$HOME"/.utils/rubinius/bin"
PATH+=":"$HOME"/.cabal/bin"
