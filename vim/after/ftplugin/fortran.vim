setlocal sw=2 ts=2
echo "doing fortran!"

function PrintTemplate()
    ruby << EOF
        insert = "print*, \""
        line, col = Vim::Window.current.cursor
        Vim::Buffer.current[line] = ' '*(col+1) + insert
        Vim::Window.current.cursor = [line, (col+1) + insert.length]
EOF
endfunction
map <leader>d :call PrintTemplate()<CR>

