"Vim syntax file
"Language: nat ep's pseudocode
"Maintainer: Nat Egan-Pimblett
"Latest Revision: 11 February 2013

if exists("b:current_syntax")
    finish
endif

" keywords
syn keyword ps_statement if while call etc use ie

" Matches
"syn match syntaxElementMatch 'regexp' contains=syntaxElement1 \
"nextgroup=syntaxElement2 skipwhite
syn match ps_comment '#.*$'
syn match nota_benne '\([^*]\|^\)\*[^*].*[^*]\*\([^*]\|$\)'
syn match string '".*"'
syn match actionables '\.\.\.\|wtf'
syn match literal_source '\'.*\''

" Regions
"syn region syntaxElementRegion start='x' end='y'


"Highlighting
hi ps_comment ctermfg=8
hi nota_benne ctermfg=7 ctermbg=19
hi actionables ctermfg=1
hi string ctermfg=22
hi literal_source ctermfg=29
hi def link ps_statement Statement
